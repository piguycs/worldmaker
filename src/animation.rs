use crate::*;

#[derive(Component, Deref, DerefMut)]
pub struct AnimationTimer(Timer);

#[derive(Component)]
pub struct AnimationIndex {
    pub first: usize,
    pub last: usize,
}

#[derive(Bundle)]
pub struct AnimationBundle(AnimationTimer, AnimationIndex);

impl AnimationBundle {
    pub fn new(timer: Timer, first: usize, last: usize) -> Self {
        Self(AnimationTimer(timer), AnimationIndex { first, last })
    }
}

fn animate_sprite(
    time: Res<Time>,
    mut query: Query<(&mut TextureAtlas, &mut AnimationTimer, &mut AnimationIndex)>,
) {
    for (mut atlas, mut timer, index) in &mut query {
        timer.tick(time.delta());

        if timer.just_finished() {
            if atlas.index == index.last {
                atlas.index = index.first;
            } else {
                atlas.index += 1;
            }
        }
    }
}

pub struct AnimationPlugin;

impl Plugin for AnimationPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, animate_sprite);
    }
}
