use crate::*;

#[derive(Component, Default)]
pub struct Collider {
    pub hit: bool,
}

#[derive(Component, Deref, DerefMut)]
pub struct Size(Vec2);

#[derive(Bundle)]
pub struct ColliderBundle(Collider, Size);

#[derive(Component)]
pub struct Movable;

impl ColliderBundle {
    pub fn new(size: Vec2) -> Self {
        Self(Collider::default(), Size(size))
    }
}

#[derive(Default, Reflect, GizmoConfigGroup)]
struct BasicSquareGizmo;

pub struct ColliderPlugin;

fn show_gizmo(
    mut gizmos: Gizmos<BasicSquareGizmo>,
    objects: Query<(&Size, &Transform, &Collider)>,
    dbg_mode: Res<DebugMode>,
) {
    if !dbg_mode.0 {
        return;
    }

    for (size, position, collider) in &objects {
        let colour = if collider.hit {
            Color::RED
        } else {
            Color::GREEN
        };
        gizmos.rect_2d(position.translation.xy(), 0.0, size.0, colour)
    }
}

fn calculate_colissions(
    mut objects: Query<(&Size, &Transform, &mut Collider), Without<Movable>>,
    mut entities: Query<(&Size, &Transform, &mut Collider), With<Movable>>,
) {
    for (size, transform, mut collider) in &mut entities {
        let exr = transform.translation.x + (size.x / 2.0);
        let exl = transform.translation.x - (size.x / 2.0);
        let eyt = transform.translation.y + (size.y / 2.0);
        let eyb = transform.translation.y - (size.y / 2.0);
        for (w_size, w_transform, mut w_collider) in &mut objects {
            let xr = w_transform.translation.x + (w_size.x / 2.0);
            let xl = w_transform.translation.x - (w_size.x / 2.0);
            let yt = w_transform.translation.y + (w_size.y / 2.0);
            let yb = w_transform.translation.y - (w_size.y / 2.0);

            if exr > xl && exl < xr && eyt > yb && eyb < yt {
                collider.hit = true;
                w_collider.hit = true;

                return;
            }

            collider.hit = false;
            w_collider.hit = false;
        }
    }
}

impl Plugin for ColliderPlugin {
    fn build(&self, app: &mut App) {
        app.init_gizmo_group::<BasicSquareGizmo>();
        app.add_systems(Update, (show_gizmo, calculate_colissions));
    }
}
