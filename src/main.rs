mod player;
mod tile;

pub mod animation;
pub mod collider;
pub mod resfile;

#[doc(hidden)]
pub use bevy::prelude::*;
use bevy_inspector_egui::prelude::*;

use bevy::window::PrimaryWindow;
use bevy_inspector_egui::bevy_egui::{EguiContext, EguiPlugin};
use bevy_inspector_egui::inspector_options::ReflectInspectorOptions;
use bevy_inspector_egui::{bevy_inspector, egui, DefaultInspectorConfigPlugin};
use serde::{Deserialize, Serialize};

use collider::ColliderBundle;
use player::PlayerPlugin;

// resource loading
use resfile::ResFile;
use worldmaker_derive::res;

pub const PLAYER_NAME: &str = "player";
pub const PLAYER_SCALE: [f32; 3] = [3.0, 3.0, 3.0];

#[derive(Debug, Resource, Default, Deref, DerefMut)]
pub struct DebugMode(bool);

#[res("player.ron")]
pub struct PlayerConfig {
    #[inspector(min = 0.0, max = 1000.0)]
    speed: f32,
    #[inspector(min = 0, max = 65_535)]
    health: u16,
}

fn spawn_camera(mut cmd: Commands) {
    cmd.spawn(Camera2dBundle::default());
}

fn spawn_maze(mut cmd: Commands) {
    let mut pos = Transform::default();

    for y in 2..7 {
        let y = 72.0 * y as f32;
        pos.translation = Vec3::new(0.0, y, 0.0);
        cmd.spawn((ColliderBundle::new(Vec2::new(72.0, 72.0)), pos));
    }
}

fn toggle_debug_mode(mut debug_mode: ResMut<DebugMode>, input: Res<ButtonInput<KeyCode>>) {
    if input.just_pressed(KeyCode::Escape) {
        debug_mode.0 = !debug_mode.0;
    }
}

fn show_debug_ui(world: &mut World) {
    let dbg_mode = world.resource::<DebugMode>();
    if !dbg_mode.0 {
        return;
    }

    let mut ctx = world
        .query_filtered::<&mut EguiContext, With<PrimaryWindow>>()
        .single(world)
        .clone();

    egui::Window::new("Player config").show(ctx.get_mut(), |ui| {
        bevy_inspector::ui_for_resource::<PlayerConfig>(world, ui);

        if ui.button("Save").clicked() {
            world.resource::<PlayerConfig>().save();
        }
    });

    egui::Window::new("World inspector").show(ctx.get_mut(), |ui| {
        bevy_inspector::ui_for_world_entities(world, ui);
    });
}

fn main() {
    App::new()
        .init_resource::<DebugMode>()
        .init_resource::<PlayerConfig>()
        .register_type::<PlayerConfig>()
        .add_plugins((
            // external plugins
            DefaultPlugins.set(ImagePlugin::default_nearest()),
            EguiPlugin,
            DefaultInspectorConfigPlugin,
            // plugins related to the game
            PlayerPlugin,
            animation::AnimationPlugin,
            collider::ColliderPlugin,
        ))
        .add_systems(Startup, (spawn_camera, spawn_maze))
        .add_systems(Update, (toggle_debug_mode, show_debug_ui))
        .run();
}
