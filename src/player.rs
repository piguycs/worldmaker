use crate::*;

use crate::animation::AnimationBundle;
use crate::collider::{Collider, ColliderBundle};

use self::collider::Movable;

pub struct PlayerPlugin;

#[derive(Component)]
struct Player;

fn spawn_player(
    mut cmd: Commands,
    assets: Res<AssetServer>,
    mut tex_atlas_layouts: ResMut<Assets<TextureAtlasLayout>>,
) {
    let pos = Vec2::ZERO;
    let transform = Transform {
        scale: Vec3::from_array(PLAYER_SCALE),
        translation: pos.extend(0.0),
        ..default()
    };

    let texture = assets.load("characters/tilemap-characters_packed.png");
    let layout = TextureAtlasLayout::from_grid(Vec2::new(24.0, 24.0), 2, 1, None, None);
    let layout = tex_atlas_layouts.add(layout);

    cmd.spawn((
        SpriteSheetBundle {
            texture,
            atlas: TextureAtlas {
                layout,
                ..default()
            },
            transform,
            ..default()
        },
        AnimationBundle::new(Timer::from_seconds(0.4, TimerMode::Repeating), 0, 1),
        Player,
        ColliderBundle::new(Vec2::new(72.0, 72.0)),
        Movable,
        Name::from(PLAYER_NAME),
    ));
}

fn calculate_displacement(input: &KeyCode) -> Vec2 {
    match input {
        KeyCode::KeyW => Vec2::new(0.0, 1.0),
        KeyCode::KeyS => Vec2::new(0.0, -1.0),
        KeyCode::KeyA => Vec2::new(-1.0, 0.0),
        KeyCode::KeyD => Vec2::new(1.0, 0.0),
        _ => Vec2::ZERO,
    }
}

fn move_player(
    mut query: Query<(&mut Transform, &mut Collider), With<Player>>,
    input: Res<ButtonInput<KeyCode>>,
    time: Res<Time>,
    config: Res<PlayerConfig>,
) {
    // get only first, as there shall be only one
    let (mut transform, _) = query.single_mut();

    let displacement = input
        .get_pressed()
        .map(calculate_displacement)
        .sum::<Vec2>();

    // delta time componsates for time in between frames
    // makes the speed independent of the fps
    let speed = time.delta_seconds() * config.speed;

    let mut old = transform.translation;
    old += displacement.extend(0.0) * speed;

    transform.translation = old;
}

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, spawn_player);
        app.add_systems(Update, move_player);
    }
}
