use std::fs;

use serde::Serialize;

pub use worldmaker_derive::ResFile;

pub trait ResFile: Serialize {
    fn save_path() -> String;

    fn save(&self) {
        let config = ron::to_string(self).expect("Unable to parse contents into player.ron");
        let respath = format!("resources/{}", Self::save_path());

        // TODO: Show the error in the game window
        fs::write(respath, config).unwrap();
    }
}
