use proc_macro::TokenStream;

use quote::quote;
use syn::parse_macro_input;

fn extract_respath(attrs: &[syn::Attribute]) -> Option<String> {
    if let syn::Meta::NameValue(value) = &attrs.first()?.meta {
        if let syn::Expr::Lit(value) = &value.value {
            if let syn::Lit::Str(value) = &value.lit {
                return Some(value.value());
            }
        }
    }
    None
}

fn impl_resfile(ast: &syn::DeriveInput, respath: String) -> TokenStream {
    let name = ast.ident.clone();
    let gen = quote! {
        impl ResFile for #name {
            fn save_path() -> String {
                return String::from(#respath);
            }
        }
        impl Default for #name {
            fn default() -> Self {
                let config = std::fs::read_to_string(format!("resources/{}", Self::save_path()))
                    .expect("could not read resource file");

                // TODO: Proper error handeling
                // let err = format!("Could not load {}", Self::save_path());
                let config = ron::from_str(&config);

                config.unwrap()
            }
        }
    };

    gen.into()
}

#[proc_macro_derive(ResFile, attributes(respath))]
pub fn resfile_derive(input: TokenStream) -> TokenStream {
    let ast: syn::DeriveInput = syn::parse(input).unwrap();
    let attr = extract_respath(&ast.attrs).expect(
        "Could not parse respath. Be sure to use the #[respath = \"example.ron\"] attribute",
    );

    impl_resfile(&ast, attr)
}

/// derives Debug, ResFile, Resource, Reflect, InspectorOptions, Serialize, Deserialize
#[proc_macro_attribute]
pub fn res(args: TokenStream, item: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(item as syn::ItemStruct);
    let args = parse_macro_input!(args as syn::LitStr);

    let path = args.value();

    quote! {
        #[derive(Debug, ResFile, Resource, Reflect, InspectorOptions, Serialize, Deserialize)]
        #[respath = #path]
        #[reflect(Resource, InspectorOptions)]
        #ast
    }
    .into()
}
